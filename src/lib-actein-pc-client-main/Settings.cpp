#include "Settings.h"

namespace actein
{
    const std::string Settings::STEAM_EXE_NAME = u8"Steam.exe";
    const std::string Settings::STEAM_VR_EXE_NAME = u8"vrmonitor.exe";
    const std::string Settings::STEAM_VR_DASHBOARD_EXE_NAME = u8"vrdashboard.exe";
    const std::string Settings::VR_TUTORIAL_SUB_PATH = u8"steamapps\\common\\SteamVR\\tools\\steamvr_tutorial\\";
    const std::string Settings::VR_TUTORIAL_EXE_NAME = u8"steamvr_tutorial.exe";
}